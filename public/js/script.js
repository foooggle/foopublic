$(function(){

	$('.btnTambahData').on('click', function(){
		$('#exampleModalLabel').html('Tambah Data Produk');
		$('.modal-footer button[type=submit]').html('Tambah Data');
	});

	$('.tampilModalUbah').on('click', function(){
		$('#exampleModalLabel').html('Ubah Data Produk');
		$('.modal-footer button[type=submit]').html('Ubah Data');
		$('.modal-body form').attr('action','http://localhost/phpmvc/public/product/ubah');

		const id = $(this).data('id');
		// console.log(id);

		$.ajax({
			url: 'http://localhost/phpmvc/public/product/getubah',
			data: {id : id},
			method: 'post',
			dataType: 'json',
			success: function(data){
				$('#namaProduk').val(data.nama_produk); // jquery carikan saya element html dengan id namaProduk dan ubah nilainya/ambil nilai name nya
				$('#nomor_produk').val(data.nomor_produk);
				$('#kategori_produk').val(data.kategori_produk);
				$('#berat_produk').val(data.berat_produk);
				$('#harga_produk').val(data.harga_produk);
				$('#id').val(data.id);
				$('#updated_at').val(data.updated_at);
			}
		});
	});
});