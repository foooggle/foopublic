<?php

class Product extends Controller {
	public function index(){

		$data['judul'] = "Products Page";
		$data['product'] = $this->model('Product_model')->getAllProduct();

		$this->view('template/header', $data);
		$this->view('product/index', $data);
		$this->view('template/footer');
	}

	public function detail($id){

		$data['judul'] = "Detail Products Page";
		$data['product'] = $this->model('Product_model')->getProductById($id);

		$this->view('template/header', $data);
		$this->view('product/detail', $data);
		$this->view('template/footer');
		// var_dump($this->model('Product_model'));
	}

	public function tambah_data(){
		if($this->model('Product_model')->tambahDataProduk($_POST) > 0){

			Flasher::setFlash('Berhasil', 'ditambahkan', 'success');
			header('Location: ' . BASEURL . '/product');
			exit;
		}else{
			Flasher::setFlash('Gagal', 'ditambahkan', 'danger');
			header('Location: ' . BASEURL . '/product');
			exit;
		}
	}
	
	public function delete($id){
		if($this->model('Product_model')->hapusDataProduk($id) > 0){ // ketika insert baru $_POST atau $_GET

			Flasher::setFlash('Berhasil', 'dihapus', 'success');
			header('Location: ' . BASEURL . '/product');
			exit;
		}else{
			Flasher::setFlash('Gagal', 'dihapus', 'danger');
			header('Location: ' . BASEURL . '/product');
			exit;
		}
	}


	public function getubah(){
		echo json_encode($this->model('Product_model')->getProductById($_POST['id']));
	}

	public function ubah(){
		if($this->model('Product_model')->ubahDataProduk($_POST) > 0){

			Flasher::setFlash('Berhasil', 'diubah', 'success');
			header('Location: ' . BASEURL . '/product');
			exit;
		}else{
			Flasher::setFlash('Gagal', 'diubah', 'danger');
			header('Location: ' . BASEURL . '/product');
			exit;
		}
	}
}