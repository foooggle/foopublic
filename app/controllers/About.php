<?php
class About extends Controller {


	public function index($nama = "..", $profesi = "..", $umur = 0)
	{
		$data['nama']	= $nama;
		$data['profesi']= $profesi;
		$data['umur']	= $umur;
		$data['judul'] = "About";

		$this->view('template/header', $data);
		$this->view('about/index', $data);
		$this->view('template/footer');
	}
	public function page(){
		$this->view('template/header');
		$this->view('about/page');
		$this->view('template/footer');
	}
}