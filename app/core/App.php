<?php

class App {

	protected $controller 	= 'Home';
	protected $method		= 'index';
	protected $params		= [];
	
	function __construct(){
		// var_dump($_GET);
		$url = $this->parserURL();
		// print_r($url);
		// controller "Home[0]/index[1]/satu[2]/dua[3]"
		if (file_exists('../app/controllers/'.$url[0].'.php')) {
			$this->controller = $url[0]; // lihat property
			unset($url[0]);
		}

		require_once '../app/controllers/' . $this->controller . '.php';
		$this->controller = new $this->controller;

		//method
		if (isset($url[1])) {
			if (method_exists($this->controller, $url[1])) {
				$this->method  = $url[1]; // lihat property
				unset($url[1]);
			}
		}
		//params
		if (!empty($url)) {
			$this->params = array_values($url);
			// var_dump($this->params = array_values($url));
		}
		// jalankan controller dan method, serta kirimkan params jika ada, menggunakan fungsi call_user_func_array
		call_user_func_array([$this->controller, $this->method], $this->params);
		// var_dump($a);
	}

	public function parserURL(){
		if (isset($_GET['url'])) {
			$url = rtrim($_GET['url'], '/');
			$url = filter_var($url, FILTER_SANITIZE_URL);
			$url = explode('/', $url);
			return $url;
		}
	}
}