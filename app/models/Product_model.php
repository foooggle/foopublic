<?php

class Product_model{

	private $table = 'is_product';
	private $db;

	public function __construct(){
		$this->db = new Database;
	}

	public function getAllProduct(){
		// $this->stmt = $this->db->prepare('SELECT * FROM is_product');
		// $this->stmt->execute();
		// return $this->stmt->fetchAll(PDO::FETCH_ASSOC);

		$this->db->query('SELECT * FROM '.$this->table);
		return $this->db->resultSet();
	}

	public function getProductById($id)
	{
		$this->db->query("SELECT * FROM ". $this->table ." WHERE id=:id");
		$this->db->bind('id', $id);
		return $this->db->singleSet();
	}

	public function tambahDataProduk($data){
		$query = "INSERT INTO is_product VALUES ('', :nama_produk, :nomor_produk, :kategori_produk, :berat_produk, :harga_produk, NOW(), NOW())";
		$this->db->query($query);
		$this->db->bind('nama_produk', $data['nama_produk']);
		$this->db->bind('nomor_produk', $data['nomor_produk']);
		$this->db->bind('kategori_produk', $data['kategori_produk']);
		$this->db->bind('berat_produk', $data['berat_produk']);
		$this->db->bind('harga_produk', $data['harga_produk']);

		$this->db->execute();
		return $this->db->rowCount();
		// return 0;
	}

	public function hapusDataProduk($id){
		$query = "DELETE FROM is_product WHERE id=:id";
		$this->db->query($query);
		$this->db->bind('id', $id);

		$this->db->execute();
		return $this->db->rowCount();
		// return 0;
	}

	public function ubahDataProduk($data){
		$query = "UPDATE is_product SET 
				 nama_produk 		= :nama_produk,
				 nomor_produk		= :nomor_produk, 
				 kategori_produk	= :kategori_produk, 
				 berat_produk		= :berat_produk,
				 harga_produk		= :harga_produk, 
				 updated_at			= NOW()
				 WHERE id 			= :id";
		$this->db->query($query);
		$this->db->bind('id', $data['id']);
		$this->db->bind('nama_produk', $data['nama_produk']);
		$this->db->bind('nomor_produk', $data['nomor_produk']);
		$this->db->bind('kategori_produk', $data['kategori_produk']);
		$this->db->bind('berat_produk', $data['berat_produk']);
		$this->db->bind('harga_produk', $data['harga_produk']);

		$this->db->execute();
		return $this->db->rowCount();
	}
}