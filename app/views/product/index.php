<div class="container mt-2">
<div class="row">
	<div class="col-lg-6">
		<?php  Flasher::doFlash();  ?>
	</div>
</div>
	<div class="row">
		<div class="col-lg-6">
			<h2>List Of Our Product</h2>
			<?php foreach($data['product'] as $p): ?>
			<ul class="list-group">
				<li class="list-group-item mb-1">
					Nama Produk:<?=$p['nama_produk']?> <br>
					Updated at:
					<small class="text-primary"><?=$p['updated_at'];?></small>
					<a href="<?= BASEURL; ?>/product/delete/<?= $p['id'] ?>" class="badge badge-danger float-right ml-2" onclick="return confirm('Yakin menghapus data?')">
						Delete
					</a>
					<a href="<?= BASEURL; ?>/product/detail/<?= $p['id'] ?>" class="badge badge-warning float-right ml-2">
						See Detail!
					</a>
					<a href="<?= BASEURL; ?>/product/ubah/<?= $p['id'] ?>" class="badge badge-primary float-right tampilModalUbah" data-toggle="modal" data-target="#exampleModal" data-id="<?=$p['id']?>">
						Edit
					</a>
				</li>
			</ul>
			<?php endforeach;?>

			<!-- Button trigger modal -->
			<button type="button" class="btn btn-primary btn-block btnTambahData" data-toggle="modal" data-target="#exampleModal">
				Tambah Data
			</button>
			<!-- Button trigger modal -->
		</div>
	</div>
</div>

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
	aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form action="<?= BASEURL;?>/product/tambah_data" method="post">
					<input type="hidden" id="id" name="id">
					<input type="hidden" id="updated_at" name="updated_at">
					<div class="form-group">
						<label for="nama_produk">Nama Produk</label>
						<input type="text" class="form-control" id="namaProduk" name="nama_produk"
							placeholder="John Doe" required>
					</div>
					<div class="form-group">
						<label for="nomor_produk">Nomor Produk</label>
						<input type="text" class="form-control" id="nomor_produk" name="nomor_produk"
							placeholder="123SDFSD" required>
					</div>
					<div class="form-group">
						<label for="kategori_produk">Kategori Produk</label>
						<select class="form-control" id="kategori_produk" name="kategori_produk">
							<option value="null">-</option>
							<option value="Souvenir">Souvenir</option>
							<option value="Gift">Gift</option>
							<option value="Hobby">Hobby</option>
							<option value="Box">Box</option>
						</select>
					</div>
					<div class="form-group">
						<label for="berat_produk">Berat Produk</label>
						<input type="text" class="form-control" id="berat_produk" name="berat_produk"
							placeholder="23 gram" required>
					</div>
					<div class="form-group">
						<label for="harga_produk">Harga Produk</label>
						<input type="text" class="form-control" id="harga_produk" name="harga_produk"
							placeholder="$230" required>
					</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
				<button type="submit" class="btn btn-primary">Tambah Data</button>
			</div>
			</form>
		</div>
	</div>
</div>
</div>