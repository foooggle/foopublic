<div class="container mt-2">
<div class="card" style="width: 18rem;">
	<div class="card-body">
	<h5 class="card-title"><?= $data['product']['nama_produk'] ?></h5>
	<h6 class="card-subtitle mb-2 text-muted"><?= $data['product']['harga_produk'] ?></h6>
	<p class="card-text"><?=$data['product']['nomor_produk']?></p>
	<p class="card-text"><?=$data['product']['kategori_produk']?></p>
	<p class="card-text"><?=$data['product']['berat_produk']."<br>". $data['product']['created_at']; ?></p>
	<a href="#" class="card-link">Card link</a>
	<a href="#" class="card-link">Another link</a>
</div>
</div>
</div>